drop schema if exists schooldb;

create schema schooldb;

use schooldb;

create table client(
id int not null auto_increment primary key,
name char(45),
address char(45),
email char(45),
age int );

create table product(
id int not null auto_increment primary key,
name char(45),
category char(45),
price int );

create table ord(
id int not null auto_increment primary key,
oName char(45) );

insert into client(id, name, address, email, age) values
(1, 'Maria', 'aaa', 'a1@ceva.co', 25),
(2, 'Bogdan', 'bbb', 'b1@altceva.co', 35),
(3, 'Ionescu', 'ccc', 'c1@merge.co', 22),
(4, 'Ana', 'ddd', 'd1@bun.co', 12);

insert into product(id, name, category, price) values
(1, 'shoes', 'sport', 500),
(2, 'egg', 'food', 250),
(3, 'fishing', 'recreation', 900);
