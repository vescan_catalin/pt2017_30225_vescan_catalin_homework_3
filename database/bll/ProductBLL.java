package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import bll.validators.ProductPriceValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Product;

public class ProductBLL {

	private List<Validator<Product>> validators;

	public ProductBLL() {
		validators = new ArrayList<Validator<Product>>();
		validators.add(new ProductPriceValidator());
	}

	public Product findProductById(int id) {
		Product p = ProductDAO.findById(id);
		if (p == null) {
			throw new NoSuchElementException("The Product with id =" + id + " was not found!");
		}
		return p;
	}

	public int insertProduct(Product Product) {
		for (Validator<Product> v : validators) {
			v.validate(Product);
		}
		return ProductDAO.insert(Product);
	}
}
