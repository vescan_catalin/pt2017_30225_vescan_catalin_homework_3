package bll.validators;

import model.Product;

public class ProductPriceValidator implements Validator<Product> {
	private static final int MIN_PRICE = 0;
	private static final int MAX_PRICE = 1000;

	public void validate(Product t) {

		if (t.getPrice() < MIN_PRICE || t.getPrice() > MAX_PRICE) {
			throw new IllegalArgumentException("The Price limit is not respected!");
		}

	}

}
