package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import connection.ConnectionFactory;
import model.Product;

public class ProductDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO schooldb.product (name,category,price)" + " VALUES (?,?,?)";
	private static final String findStatementString = "SELECT * FROM schooldb.product where id = ?";
	private static final String showStatementString = "SELECT * FROM schooldb.product";
	private static final String deleteStatement = "DELETE FROM schooldb.product WHERE id = ?";
	
	public static Product findById(int ProductId) {
		Product toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, ProductId);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("name");
			String category = rs.getString("category");
			int price = rs.getInt("price");
			toReturn = new Product(ProductId, name, category, price);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static int insert(Product Product) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, Product.getName());
			insertStatement.setString(2, Product.getCategory());
			insertStatement.setInt(3, Product.getPrice());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	public static ArrayList<Product> showProducts()
	   {
		ArrayList<Product> Product = new ArrayList<Product>();
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement listareStatement = null;
		ResultSet rs = null;
		try{
			listareStatement = conn.prepareStatement(showStatementString, Statement.RETURN_GENERATED_KEYS);
			rs = listareStatement.executeQuery();
			
			while(rs.next()){
				Product c = new Product(rs.getInt("id"), rs.getString("name"), rs.getString("category"), rs.getInt("price"));
				Product.add(c);
			}
		}catch(SQLException e){
			LOGGER.log(Level.WARNING, "ProductDAO:showProducts" + e.getMessage());
		}finally{	
			ConnectionFactory.close(rs);
			ConnectionFactory.close(listareStatement);
			ConnectionFactory.close(conn);	
		}
		return Product;
	}
	
	public static List<Product> remove(Product Product) throws Exception {

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
	
		try {
			findStatement = dbConnection.prepareStatement(deleteStatement);
			findStatement.setInt(1, Product.getId());
			findStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
		}
		
		ArrayList<Product> product = new ArrayList<Product>();
		PreparedStatement listareStatement = null;
		try{
			listareStatement = dbConnection.prepareStatement(showStatementString, Statement.RETURN_GENERATED_KEYS);
			rs = listareStatement.executeQuery();
			
			while(rs.next()){
				Product c = new Product(rs.getInt("id"), rs.getString("name"), rs.getString("category"), rs.getInt("price"));
				product.add(c);
			}
		}catch(SQLException e){
			LOGGER.log(Level.WARNING, "ProductDAO:showProducts" + e.getMessage());
		}finally{	
			ConnectionFactory.close(rs);
			ConnectionFactory.close(listareStatement);
			ConnectionFactory.close(dbConnection);	
		}
		return product;

	}
	
}
