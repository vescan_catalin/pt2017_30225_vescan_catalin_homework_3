package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import connection.ConnectionFactory;
import model.Client;

public class ClientDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO schooldb.client (name,address,email,age)" + " VALUES (?,?,?,?)";
	private static final String findStatementString = "SELECT * FROM schooldb.client where id = ?";
	private static final String showStatementString = "SELECT * FROM schooldb.client";
	private static final String deleteStatement = "DELETE FROM schooldb.client WHERE id = ?";
	
	public static Client findById(int clientId) {
		Client toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, clientId);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("name");
			String address = rs.getString("address");
			String email = rs.getString("email");
			int age = rs.getInt("age");
			toReturn = new Client(clientId, name, address, email, age);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static int insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, client.getName());
			insertStatement.setString(2, client.getAddress());
			insertStatement.setString(3, client.getEmail());
			insertStatement.setInt(4, client.getAge());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	public static ArrayList<Client> showClients()
	   {
		ArrayList<Client> clienti = new ArrayList<Client>();
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement listareStatement = null;
		ResultSet rs = null;
		try{
			listareStatement = conn.prepareStatement(showStatementString, Statement.RETURN_GENERATED_KEYS);
			rs = listareStatement.executeQuery();
			
			while(rs.next()){
				Client c = new Client(rs.getInt("id"),rs.getString("name"),rs.getString("address"),rs.getString("email"),rs.getInt("age"));
				clienti.add(c);
			}
		}catch(SQLException e){
			LOGGER.log(Level.WARNING, "ClientDAO:showClients" + e.getMessage());
		}finally{	
			ConnectionFactory.close(rs);
			ConnectionFactory.close(listareStatement);
			ConnectionFactory.close(conn);	
		}
		return clienti;
	}
	
	public static List<Client> remove(Client client) throws Exception {

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
	
		try {
			findStatement = dbConnection.prepareStatement(deleteStatement);
			findStatement.setInt(1, client.getId());
			findStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
		}
		
		ArrayList<Client> clienti = new ArrayList<Client>();
		PreparedStatement listareStatement = null;
		try{
			listareStatement = dbConnection.prepareStatement(showStatementString, Statement.RETURN_GENERATED_KEYS);
			rs = listareStatement.executeQuery();
			
			while(rs.next()){
				Client c = new Client(rs.getInt("id"),rs.getString("name"),rs.getString("address"),rs.getString("email"),rs.getInt("age"));
				clienti.add(c);
			}
		}catch(SQLException e){
			LOGGER.log(Level.WARNING, "ClientDAO:showClients" + e.getMessage());
		}finally{	
			ConnectionFactory.close(rs);
			ConnectionFactory.close(listareStatement);
			ConnectionFactory.close(dbConnection);	
		}
		return clienti;

	}
	
}
