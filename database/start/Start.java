package start;

/*
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
*/
import java.sql.SQLException;
import java.util.logging.Logger;
import presentation.ClientView;
import presentation.ProductView;

public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

	public static void main(String[] args) throws SQLException {

	/*
	// verificare conexiune baza de date
	Statement stmt = null;
	Connection conn = null;	
	try{
			//Class.forName(DRIVER); //deschide conexiunea
			conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/schooldb?autoReconnect=true&useSSL=false","root","root");
			
			stmt=conn.createStatement();
			String sql;
			sql="SELECT * FROM client";
			ResultSet rs=stmt.executeQuery(sql);
			while(rs.next()){
				int id  = rs.getInt("id");
				String nume = rs.getString("name");
		        String adresa = rs.getString("address");
		        String nrTelefon = rs.getString("email");
		        String oras = rs.getString("age");
		        
		        System.out.print("IDCustomer: " + id);
		        System.out.print(", Nume: " + nume);
		        System.out.print(", Adresa: " + adresa);
		        System.out.print(", Telefon: " +  nrTelefon);
		        System.out.println(", Oras: " + oras);
			}
			 rs.close();
		     stmt.close();
		     conn.close();
			
		}catch(SQLException e){
			LOGGER.log(Level.WARNING,"Eroare la conectarea bazei de date.");
			e.printStackTrace();
		} 
		*/
		
		new ClientView(1);
		new ProductView(1);
		
	}
	
}
