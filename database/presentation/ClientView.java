package presentation;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import dao.ClientDAO;
import model.Client;

public class ClientView {
	
	JFrame window;
	JPanel panel;
	JButton viewClients, addClients, removeClients;
	JLabel id, name, address, email, age;
	JTextField idText, nameText, addressText, emailText, ageText;
	JScrollPane scroll;
	JTable table;
	static DefaultTableModel model;
	
	public ClientView(int unu) {
		new ClientController();
	}
	
	public ClientView() {
		
		// create window
		window = new JFrame("Client");
		window.setSize(800, 600);
		window.setLocationRelativeTo(null);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);
		
		// create panels
		panel = new JPanel();
		panel.setLayout(null);
		
		// create buttons
		viewClients = new JButton("viewClients");
		viewClients.setBounds(30, 20, 130, 30);
		addClients = new JButton("addClients");
		addClients.setBounds(190, 20, 130, 30);
		removeClients = new JButton("removeClients");
		removeClients.setBounds(350, 20, 130, 30);
		
		// create label
		id = new JLabel("id");
		id.setBounds(30, 60, 20, 20);
		name = new JLabel("name");
		name.setBounds(100, 60, 50, 20);
		address = new JLabel("address");
		address.setBounds(270, 60, 50, 20);
		email = new JLabel("email");
		email.setBounds(450, 60, 50, 20);
		age = new JLabel("age");
		age.setBounds(670, 60, 30, 20);
		
		// create text fields
		idText = new JTextField();
		idText.setBounds(50, 60, 30, 20);
		nameText = new JTextField();
		nameText.setBounds(150, 60, 100, 20);
		addressText = new JTextField();
		addressText.setBounds(320, 60, 100, 20);
		emailText = new JTextField();
		emailText.setBounds(500, 60, 150, 20);
		ageText = new JTextField();
		ageText.setBounds(700, 60, 30, 20);
		
		// create table
		table = new JTable();
		Object[] columns = {"id", "name", "address", "email", "age"};
		
		List<Client> clientList = new ArrayList<Client>();
		clientList = ClientDAO.showClients();
		
		Object[][] data = new Object[20][20];
		int nrClient = 0;
		
		for(Client p:clientList){
			data[nrClient][0] = p.getId(); //data[rand][coloana]
			data[nrClient][1] = p.getName();
			data[nrClient][2] = p.getAddress();
			data[nrClient][3] = p.getEmail();
			data[nrClient][4] = p.getAge();
			nrClient++;
		}
		DefaultTableModel model = new DefaultTableModel(data, columns);
		table.setModel(model);
			
		Font font = new Font("", 1, 15);
		table.setFont(font);
		table.setRowHeight(30);
	
		scroll = new JScrollPane(table);
		scroll.setBounds(15, 100, 600, 440);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.setVisible(true);
		table.setVisible(true);
		
		// add buttons
		panel.add(viewClients);
		panel.add(addClients);
		panel.add(removeClients);
		panel.add(id);
		panel.add(name);
		panel.add(address);
		panel.add(email);
		panel.add(age);
		panel.add(idText);
		panel.add(nameText);
		panel.add(addressText);
		panel.add(emailText);
		panel.add(ageText);
		panel.add(scroll);
		
		// add panel
		window.add(panel);
		
		window.setVisible(true);
		
	}
		
}
