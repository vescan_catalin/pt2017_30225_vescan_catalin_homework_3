package presentation;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import dao.ProductDAO;
import model.Product;

public class ProductView {
	
	JFrame window;
	JPanel panel;
	JButton viewProducts, addProducts, removeProducts;
	JLabel id, name, category, price;
	JTextField idText, nameText, categoryText, priceText;
	JScrollPane scroll;
	JTable table;
	static DefaultTableModel model;
	
	public ProductView(int unu) {
		new ProductController();
	}
	
	public ProductView() {
		
		// create window
		window = new JFrame("Product");
		window.setSize(800, 600);
		window.setLocationRelativeTo(null);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);
		
		// create panels
		panel = new JPanel();
		panel.setLayout(null);
		
		// create buttons
		viewProducts = new JButton("viewProducts");
		viewProducts.setBounds(30, 20, 130, 30);
		addProducts = new JButton("addProducts");
		addProducts.setBounds(190, 20, 130, 30);
		removeProducts = new JButton("removeProducts");
		removeProducts.setBounds(350, 20, 130, 30);
		
		// create label
		id = new JLabel("id");
		id.setBounds(30, 60, 20, 20);
		name = new JLabel("name");
		name.setBounds(100, 60, 50, 20);
		category = new JLabel("category");
		category.setBounds(270, 60, 50, 20);
		price = new JLabel("price");
		price.setBounds(450, 60, 50, 20);
		
		// create text fields
		idText = new JTextField();
		idText.setBounds(50, 60, 30, 20);
		nameText = new JTextField();
		nameText.setBounds(150, 60, 100, 20);
		categoryText = new JTextField();
		categoryText.setBounds(330, 60, 100, 20);
		priceText = new JTextField();
		priceText.setBounds(500, 60, 150, 20);
		
		// create table
		table = new JTable();
		Object[] columns = {"id", "name", "category", "price"};
		
		List<Product> ProductList = new ArrayList<Product>();
		ProductList = ProductDAO.showProducts();
		
		Object[][] data = new Object[20][20];
		int nrProduct = 0;
		
		for(Product p:ProductList){
			data[nrProduct][0] = p.getId(); //data[rand][coloana]
			data[nrProduct][1] = p.getName();
			data[nrProduct][2] = p.getCategory();
			data[nrProduct][3] = p.getPrice();
			nrProduct++;
		}
		DefaultTableModel model = new DefaultTableModel(data, columns);
		table.setModel(model);
			
		Font font = new Font("", 1, 15);
		table.setFont(font);
		table.setRowHeight(30);
	
		scroll = new JScrollPane(table);
		scroll.setBounds(15, 100, 600, 440);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.setVisible(true);
		table.setVisible(true);
		
		// add buttons
		panel.add(viewProducts);
		panel.add(addProducts);
		panel.add(removeProducts);
		panel.add(id);
		panel.add(name);
		panel.add(category);
		panel.add(price);
		panel.add(idText);
		panel.add(nameText);
		panel.add(categoryText);
		panel.add(priceText);
		panel.add(scroll);
		
		// add panel
		window.add(panel);
		
		window.setVisible(true);
		
	}
		
}
