package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import bll.ProductBLL;
import dao.ProductDAO;
import model.Product;

public class ProductController {

	private ProductView ProductView = new ProductView();
	
	public ProductController() {
		ProductView.addProducts.addActionListener(new ActionEvents());
		ProductView.viewProducts.addActionListener(new ActionEvents());
		ProductView.removeProducts.addActionListener(new ActionEvents());
	}
	
	public void Update() {
			ProductView.table.setVisible(false);
			
			ProductBLL p = new ProductBLL();
			p.insertProduct(new Product(ProductView.nameText.getText(), ProductView.categoryText.getText(), Integer.parseInt(ProductView.priceText.getText())));
							
			Object[] columns = {"id", "name", "Category", "Price"};
			
			List<Product> ProductList = new ArrayList<Product>();
			ProductList = ProductDAO.showProducts();
			
			Object[][] data = new Object[20][20];
			int nrProduct = 0;
			
			for(Product p1:ProductList){
				data[nrProduct][0] = p1.getId(); //data[rand][coloana]
				data[nrProduct][1] = p1.getName();
				data[nrProduct][2] = p1.getCategory();
				data[nrProduct][3] = p1.getPrice();
				nrProduct++;
			}
			
			DefaultTableModel model = new DefaultTableModel(data, columns);
			ProductView.table.setModel(model);
			ProductView.table.setVisible(true);
	}
	
	public void view() {
		ProductView.table.setVisible(false);
		
		Object[] columns = {"id", "name", "Category", "Price"};
		ProductBLL p = new ProductBLL();
		Product p1 = p.findProductById(Integer.parseInt(ProductView.idText.getText()));
		
		Object[][] data = new Object[20][20];
		int nrProduct = 0;
		
		data[nrProduct][0] = p1.getId(); //data[rand][coloana]
		data[nrProduct][1] = p1.getName();
		data[nrProduct][2] = p1.getCategory();
		data[nrProduct][3] = p1.getPrice();
		nrProduct++;
		
		DefaultTableModel model = new DefaultTableModel(data, columns);
		ProductView.table.setModel(model);
		ProductView.table.setVisible(true);
	}
	
	public void remove() {
		ProductView.table.setVisible(false);
		
		ProductBLL p = new ProductBLL();
		Product toRemove = p.findProductById(Integer.parseInt(ProductView.idText.getText()));
						
		Object[] columns = {"id", "name", "Category", "Price"};
		
		List<Product> ProductList = new ArrayList<Product>();
		try {
			ProductList = ProductDAO.remove(toRemove);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		Object[][] data = new Object[20][20];
		int nrProduct = 0;
		
		for(Product p1:ProductList){
			data[nrProduct][0] = p1.getId(); //data[rand][coloana]
			data[nrProduct][1] = p1.getName();
			data[nrProduct][2] = p1.getCategory();
			data[nrProduct][3] = p1.getPrice();
			nrProduct++;
		}
		
		DefaultTableModel model = new DefaultTableModel(data, columns);
		ProductView.table.setModel(model);
		ProductView.table.setVisible(true);
	}

	class ActionEvents implements ActionListener{
		public void actionPerformed(ActionEvent e) {
	
			if(e.getSource() == ProductView.addProducts) {
				Update();
			}
			if(e.getSource() == ProductView.viewProducts) {
				view();
			}
			if(e.getSource() == ProductView.removeProducts) {
				remove();
			}
		}
	}
		
}
