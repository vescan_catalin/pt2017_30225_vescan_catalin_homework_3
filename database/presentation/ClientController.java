package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import bll.ClientBLL;
import dao.ClientDAO;
import model.Client;

public class ClientController {

	private ClientView clientView = new ClientView();
	
	public ClientController() {
		clientView.addClients.addActionListener(new ActionEvents());
		clientView.viewClients.addActionListener(new ActionEvents());
		clientView.removeClients.addActionListener(new ActionEvents());
	}
	
	public void Update() {
			clientView.table.setVisible(false);
			
			ClientBLL c = new ClientBLL();
			c.insertClient(new Client(clientView.nameText.getText(), clientView.addressText.getText(), clientView.emailText.getText(), Integer.parseInt(clientView.ageText.getText())));
							
			Object[] columns = {"id", "name", "address", "email", "age"};
			
			List<Client> clientList = new ArrayList<Client>();
			clientList = ClientDAO.showClients();
			
			Object[][] data = new Object[20][20];
			int nrClient = 0;
			
			for(Client p:clientList){
				data[nrClient][0] = p.getId(); //data[rand][coloana]
				data[nrClient][1] = p.getName();
				data[nrClient][2] = p.getAddress();
				data[nrClient][3] = p.getEmail();
				data[nrClient][4] = p.getAge();
				nrClient++;
			}
			
			DefaultTableModel model = new DefaultTableModel(data, columns);
			clientView.table.setModel(model);
			clientView.table.setVisible(true);
	}
	
	public void view() {
		clientView.table.setVisible(false);
		
		Object[] columns = {"id", "name", "address", "email", "age"};
		ClientBLL c = new ClientBLL();
		Client c1 = c.findClientById(Integer.parseInt(clientView.idText.getText()));
		
		Object[][] data = new Object[20][20];
		int nrClient = 0;
		
		data[nrClient][0] = c1.getId(); //data[rand][coloana]
		data[nrClient][1] = c1.getName();
		data[nrClient][2] = c1.getAddress();
		data[nrClient][3] = c1.getEmail();
		data[nrClient][4] = c1.getAge();
		nrClient++;
		
		DefaultTableModel model = new DefaultTableModel(data, columns);
		clientView.table.setModel(model);
		clientView.table.setVisible(true);
	}
	
	public void remove() {
		clientView.table.setVisible(false);
		
		ClientBLL c = new ClientBLL();
		Client toRemove = c.findClientById(Integer.parseInt(clientView.idText.getText()));
						
		Object[] columns = {"id", "name", "address", "email", "age"};
		
		List<Client> clientList = new ArrayList<Client>();
		try {
			clientList = ClientDAO.remove(toRemove);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		Object[][] data = new Object[20][20];
		int nrClient = 0;
		
		for(Client p:clientList){
			data[nrClient][0] = p.getId(); //data[rand][coloana]
			data[nrClient][1] = p.getName();
			data[nrClient][2] = p.getAddress();
			data[nrClient][3] = p.getEmail();
			data[nrClient][4] = p.getAge();
			nrClient++;
		}
		
		DefaultTableModel model = new DefaultTableModel(data, columns);
		clientView.table.setModel(model);
		clientView.table.setVisible(true);
	}

	class ActionEvents implements ActionListener{
		public void actionPerformed(ActionEvent e) {
	
			if(e.getSource() == clientView.addClients) {
				Update();
			}
			if(e.getSource() == clientView.viewClients) {
				view();
			}
			if(e.getSource() == clientView.removeClients) {
				remove();
			}
		}
	}
		
}
